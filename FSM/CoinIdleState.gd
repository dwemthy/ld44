extends "res://FSM/State.gd"

var is_player = false

func get_name():
	return "Idle"
	
func stop_being_player():
	print("idle told to stop being player")
	
func enter(player):
	.enter(player)
	player.x_input = 0
	if player.added:
		player.animation_player.play("Neutral")
	else:
		player.animation_player.play("Afraid")
	
func become_player():
	is_player = true
	
#warning-ignore:unused_argument
func _fixed_process(delta):
	if is_player:
		return player.PlayerState.new()