extends "res://FSM/State.gd"

var stop_being_player = false

func get_name():
	return "Player"
	
func stop_being_player():
	stop_being_player = true
	
func become_player():
	print("become_player() called on player state")
	
func _fixed_process(delta):
	var current_animation = player.animation_player.current_animation
	if player.current_velocity.y < 0:
		if not current_animation == "JumpRight" and not current_animation == "JumpLeft":
			if player.x_input < 0:
				player.animation_player.play("JumpLeft")
			else:
				player.animation_player.play("JumpRight")
	elif not player.can_jump:
		if not current_animation == "Fall":
			player.animation_player.play("Fall")
	else:
		if current_animation == "Fall":
			player.get_node("LandStream").play()
		if player.x_input > 0:
			if not player.animation_player.current_animation == "WalkRight":
				player.animation_player.play("WalkRight")
		elif player.x_input < 0:
			if not player.animation_player.current_animation == "WalkLeft":
				player.animation_player.play("WalkLeft")
		else:
			if not player.animation_player.current_animation == "Neutral":
				player.animation_player.play("Neutral")
			
	if stop_being_player:
		return player.IdleState.new()

func _input(event):
	if event.is_action_pressed("left"):
		if event is InputEventJoypadMotion:
			player.x_input = -1
		else:
			if Input.is_action_pressed("right"):
				player.x_input = 0
			else:
				player.x_input = -1
	elif event.is_action_released("left"):
		if event is InputEventJoypadMotion:
			player.x_input = 0
		else:
			if Input.is_action_pressed("right"):
				player.x_input = 1
			else:
				player.x_input = 0
		
	if event.is_action_pressed("right"):
		if event is InputEventJoypadMotion:
			player.x_input = 1
		else:
			if Input.is_action_pressed("left"):
				player.x_input = 0
			else:
				player.x_input = 1
	elif event.is_action_released("right"):
		if event is InputEventJoypadMotion:
			player.x_input = 0
		else:
			if Input.is_action_pressed("left"):
				player.x_input = -1
			else:
				player.x_input = 0
				
	if event.is_action_pressed("jump"):
		player.jump()
	
	if event.is_action_pressed("join"):
		player.join()