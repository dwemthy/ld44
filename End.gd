extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Hand.get_node("AnimationPlayer").play("Grab")
	$Hand.get_node("AnimationPlayer").playback_speed = 100