extends Camera2D

var world

var active_player

func _ready():
	world = get_node("/root/World")
	world.connect("active_player", self, "set_active_player")
	
func _process(delta):
	if active_player:
		position = active_player.global_position
		
func set_active_player(player):
	active_player = player