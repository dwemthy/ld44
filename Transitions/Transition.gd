extends Node2D

export(PackedScene) var Next

export var type = "Copper"

export var max_speed = 512
export var min_speed = 256

export var spin_speed = PI * 0.5

export var needed = 5

signal five_in

var coins = []

var sprites = {}

var camera

var joining = false

func _ready():
	$CopperChecker.connect("body_entered", self, "body_entered")
	$CopperChecker.connect("body_exited", self, "body_exited")
	camera = get_node("/root/World/Camera2D")
	
func _physics_process(delta):
	if joining:
		$Indicator.hide()
		rotate(spin_speed * delta)
		if sprites.size() > 0:
			for sprite in sprites.keys():
				var to_move = -sprite.position.normalized() * sprites[sprite] * delta
				if to_move.length() == 0 or to_move.length() > sprite.position.length():
					sprites.erase(sprite)
					sprite.queue_free()
				else:
					sprite.position += to_move
		else:
			finish_join()
	elif coins.size() == needed:
		$Indicator.show()
	else:
		$Indicator.hide()
				
func body_entered(body):
	if not joining and body.added:
		if type in body.name and coins.size() < needed:
			coins.append(body)
			if coins.size() == needed:
				emit_signal("five_in", true)
	
func body_exited(body):
	if not joining:
		if type in body.name and coins.find(body) >= 0:
			coins.remove(coins.find(body))
			if coins.size() < needed:
				emit_signal("five_in", false)
			
func join():
	if not joining:
		joining = true
		var average_position = Vector2(0,0)
		for i in range(needed - 1, -1, -1):
			coins[i].remove_from_active()
			average_position += coins[i].position
			var coin_sprites = coins[i].disassemble_sprites()
			coins[i].killed = true
			for sprite in coin_sprites:
				sprites[sprite] = rand_range(min_speed, max_speed)
		
		var tilemap = get_node("/root/World/TileMap")
		get_parent().remove_child(self)
		average_position = average_position / needed
		position = average_position
		tilemap.add_child(self)
		
		for sprite in sprites.keys():
			var pos = sprite.global_position - average_position
			sprite.get_parent().remove_child(sprite)
			sprite.position = pos
			add_child(sprite)
			
		camera.set_active_player(null)
		
func finish_join():
	var next = Next.instance()
	next.position = global_position
	get_node("/root/World/TileMap").add_child(next)
	next.add_to_active()
	next.become_player()
	get_node("/root/World").set_active_coin(next)
	
	for coin in coins:
		coin.queue_free()
		
	coins.clear()
	queue_free()