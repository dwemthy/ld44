extends Node2D

signal active_player

var active_coins = []
var active_index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("coin_master")
	
func add_active_coin(active_coin):
	active_coins.append(active_coin)
	if active_coins.size() == 1:
		active_coin.become_player()
		emit_signal("active_player", active_coin)

func remove_active_coin(active_coin):
	active_coins.remove(active_coins.find(active_coin))
	if active_index > active_coins.size():
		active_index = 0
	
func _unhandled_input(event):
	if event.is_action_pressed("switch_up"):
		switch_up()
	elif event.is_action_pressed("switch_down"):
		switch_down()
			
func switch_up():
	if active_coins.size() > 1:
		set_active_index(active_index + 1)

func set_active_coin(coin):
	var index = active_coins.find(coin)
	if index >= 0:
		set_active_index(index)
	else:
		print("asked to set coin that's not in active")
	
func switch_down():
	if active_coins.size() > 1:
		set_active_index(active_index - 1)
		
func set_active_index(index):
	active_coins[active_index].stop_being_player()
	if index < 0:
		index = active_coins.size() - 1
	elif index >= active_coins.size():
		index = 0
	active_index = index
	active_coins[active_index].become_player()
	emit_signal("active_player", active_coins[active_index])