extends KinematicBody2D

export var bounce_force = 1000

var velocity = Vector2(0,0)

func _ready():
	$SpringZone.connect("body_entered", self, "launch_body")
	
func launch_body(body):
	if body.name == name:
		return
	if "Copper" in body.name or "Silver" in body.name or "Gold" in body.name or "Plat" in body.name:
		body.push(Vector2(0, -body.jump_power * 1.5))
	elif "Marble" in body.name:
		body.push(Vector2(0, -bounce_force))
		
	if not $Sproing.playing:
		$Sproing.play()