extends KinematicBody2D

export var gravity = 2000

var velocity = Vector2(0,0)

var radius = 64
var falling = false
var last_collision = null

func _ready():
	last_collision = Vector2(0, 0) #so the landing sound doens't play on spawn

func _physics_process(delta):
	var pre_position = position
	var linear_velocity = move_and_slide(velocity)
	if get_slide_count() > 0:
		if not last_collision:
			$Land.play()
			
		last_collision = get_slide_collision(0)
	else:
		last_collision = null
		
	var travelled = position - pre_position
		
	var dist = travelled.x
	rotation += dist / radius # Radians!
	
	$Gleam.rotation = -rotation
		
	# Slow down velocity
	velocity.x = lerp(velocity.x, 0, delta)
	velocity.y = lerp(velocity.y, gravity, delta)

func push(push_force):
	velocity = push_force