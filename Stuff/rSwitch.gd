extends StaticBody2D

export var type = "Silver"

var door

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sensor.connect("body_entered", self, "body_entered")
	door = get_node("Door")

func body_entered(body):
	if type in body.name and door:
		door.open()