extends Node2D

export var type = "Plat"
export(PackedScene) var Hand

var grabbed = null
var hand

var grab_finished = false
var pull_finished = false
var pull_started = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area2D.connect("body_entered", self, "body_entered")
	
func _physics_process(delta):
	if pull_finished:
		get_tree().change_scene("res://End.tscn")
	elif grabbed:
		if grab_finished and not pull_started:
			grabbed.get_parent().remove_child(grabbed)
			grabbed.position = Vector2(0,0)
			grabbed.scale = Vector2(0.5, 0.5) #palm is scaled up by 2
			var coin_parent = hand.get_node("Palm/Target")
			coin_parent.add_child(grabbed)
			hand.get_node("AnimationPlayer").play("Pull")
			pull_started = true
		else:
			summon_hand()
	
func body_entered(body):
	if type in body.name:
		body.capture(self)
		
func grab(coin):
	grabbed = coin
		
func summon_hand():
	if Hand and not hand:
		hand = Hand.instance()
		add_child(hand)
		hand.get_node("AnimationPlayer").play("Grab")
		hand.get_node("AnimationPlayer").connect("animation_finished", self, "animation_finished")

func animation_finished(anim):
	if anim == "Grab":
		grab_finished = true
		
	if anim == "Pull":
		pull_finished = true