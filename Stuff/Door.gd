extends Node2D

export var open_to = PI * 2
export var open_speed = PI * 0.25

var opening = false

func _physics_process(delta):
	if opening:
		var remaining = open_to - rotation
		if abs(remaining) <= abs(open_speed) * delta:
			rotation = open_to
			opening = false
		else:
			rotate(open_speed * delta)

func open():
	opening = true