extends KinematicBody2D

const IdleState = preload("res://FSM/CoinIdleState.gd")
const PlayerState = preload("res://FSM/CoinPlayerState.gd")

export var speed = 100
export var jump_power = 100
export var acceleration = 0.5
export var extra_gravity = 400

export var starting_player = false

export(PackedScene) var Transition

var state

var can_jump = true

var transition

var x_input = 0
var current_velocity = Vector2(0,0)

var animation_player

var killed = false

var captor

# Called when the node enters the scene tree for the first time.
func _ready():
	animation_player = $AnimationPlayer
	
	state = IdleState.new()
	state.enter(self)
	
	$FriendZone.connect("body_entered", self, "found_friend")
	
	if starting_player:
		add_to_active()

func _physics_process(delta):
	if killed:
		return
		
	if captor:
		current_velocity = captor.position - position
		if current_velocity.length() < speed * 1 * delta:
			captor.grab(self)
			killed = true
		current_velocity = current_velocity.normalized() * speed * 1
	else:
		#simulate gravity
		var applied_gravity = extra_gravity
		# apply gravity
		current_velocity.y = lerp(current_velocity.y, applied_gravity, delta)
			
		# apply input tp x component
		var x_delta = x_input * speed
		current_velocity.x = lerp(current_velocity.x, x_delta, acceleration * delta)
		
	# do movement and handle collisions
	var remaining = move_and_slide(current_velocity)
	if get_slide_count() > 0:
		var on_floor = false
		
		for i in range(get_slide_count()):
			var collision = get_slide_collision(i)
			if collision.normal.y < 0:
				on_floor = true
				current_velocity.y = 0
			var other = collision.collider
			if "Copper" in other.name or "Silver" in other.name or "Gold" in other.name or "Marble" in other.name:
				other.push(-collision.normal * current_velocity.length() * 0.25)
				
		can_jump = on_floor
	else:
		can_jump = false
		
	_state_loop("_fixed_process", delta)
	
func push(force):
	current_velocity = force
	
func become_player():
	$Turn.play()
	state.become_player()
	if Transition:
		transition = Transition.instance()
		add_child(transition)
		transition.connect("five_in", self, "five_in")
	
func stop_being_player():
	state.stop_being_player()
	if transition:
		remove_child(transition)
		transition.queue_free()
		transition = null
	
var join_enabled = false
func join():
	if join_enabled:
		if transition:
			transition.join()
	else:
		print("can't join")
		
func capture(by):
	captor = by
	
func five_in(are_there_five):
	join_enabled = are_there_five
	
func found_friend(friend):
	if added:
		friend.add_to_active()
	
var added = false
func add_to_active():
	if not added:
		$Cheer.play()
		added = true
		animation_player.play("Cheer")
		get_node("/root/World").add_active_coin(self)
		
func remove_from_active():
	get_node("/root/World").remove_active_coin(self)
	collision_layer = 0
	collision_mask = 0
	
func jump():
	# vertical jump
	if can_jump:
		$JumpStream.play()
		current_velocity.y = -jump_power
		can_jump = false
	
# have the state handle input
func _unhandled_input(event):
	state._input(event)
		
# Call the given function with the given arg and iterate if state changes.
func _state_loop(function, arg):
    # Keep a list of old states to prevent cycles.
    var old_states = []
    var new_state = state.call(function, arg)
    while new_state != null:
        var new_state_name = new_state.get_name()
        # Throw an exception if we re-enter a previously visited state this frame.
        assert(old_states.find(new_state_name) == -1)
        old_states.append(new_state_name)
        state.set_state(new_state)
        # Let our new state run this cycle since our old state ended.
        new_state = state.call(function, arg)
		
func disassemble_sprites():
	if not $Background:
		return null
	var sprites = disassemble_sprites_from($Background)
	sprites.append($Background)
	return sprites
	
func disassemble_sprites_from(base_sprite):
	var sprites = []
	
	for i in range(base_sprite.get_child_count()):
		var child = base_sprite.get_child(i)
		var undersprites = disassemble_sprites_from(child)
		if undersprites:
			for sprite in undersprites:
				sprites.append(sprite)
		sprites.append(child)
	
	return sprites