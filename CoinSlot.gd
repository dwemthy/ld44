extends KinematicBody2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sensor.connect("body_entered", self, "body_entered")
	
func body_entered(body):
	queue_free()
	body.queue_free()